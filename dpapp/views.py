from django.urls import reverse
from django.shortcuts import render
from paypal.standard.forms import PayPalPaymentsForm
from django.http import HttpResponse


def view_that_asks_for_money(request):

    # What you want the button to do.
    paypal_dict = {
        "business": "sharry@gmail.com",
        "amount": "100.00",
        "item_name": "name of the item",
        "invoice": "4",
        "notify_url": request.build_absolute_uri(reverse('paypal-ipn')),
        "return": request.build_absolute_uri(reverse('ps')),
        "cancel_return": request.build_absolute_uri(reverse('pf')),
        "custom": "premium_plan",  # Custom command to correlate to some function later (optional)
    }

    # Create the instance.
    form = PayPalPaymentsForm(initial=paypal_dict)
    context = {"form": form}
    return render(request, "payment.html", context)

def p_success(r):
    return HttpResponse("Success")

def p_fail(r):
    return HttpResponse("Failure")
